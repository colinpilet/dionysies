import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Auteur {
    private String nom;
    private int qualiteComedie;
    private String citationComedie;
    private int qualiteTragedie;
    private String citationTragedie;
    private int qualiteDrame;
    private String citationDrame;
    
    public Auteur(String nom, int qualiteTragedie, String citationTragedie, int qualiteComedie, String citationComedie, int qualiteDrame, String citationDrame){
        this.nom = nom;
        this.qualiteComedie = qualiteComedie;
        this.citationComedie = citationComedie;
        this.qualiteTragedie = qualiteTragedie;
        this.citationTragedie = citationTragedie;
        this.qualiteDrame = qualiteDrame;
        this.citationDrame = citationDrame;
    }

    public int getQualiteTragedie(){
        return this.qualiteTragedie;
    }

    public String getCitationTragedie(){
        return this.citationTragedie;
    }

    public int getQualiteComedie(){
        return this.qualiteComedie;
    }

    public String getCitationComedie(){
        return this.citationComedie;
    }

    public int getQualiteDrame(){
        return this.qualiteDrame;
    }

    public String getCitationDrame(){
        return this.citationDrame;
    }

    public Style pointFort(){
        int max = 0;
        Style style = null;
        List<Integer> lesQualites = Arrays.asList(getQualiteComedie(), getQualiteTragedie(), getQualiteDrame());
        List<Style> lesStyles = Arrays.asList(Style.COMÉDIE, Style.TRAGÉDIE, Style.DRAME);
        for(int i = 0 ; i < lesQualites.size(); i++){
            if( lesQualites.get(i)> max){
                max = lesQualites.get(i);
                style = lesStyles.get(i);
            }
        }
        return style;
    }

    public int qualiterStyle(Style style){
        List<Integer> lesQualites = Arrays.asList(getQualiteComedie(), getQualiteTragedie(), getQualiteDrame());
        List<Style> lesStyles = Arrays.asList(Style.COMÉDIE, Style.TRAGÉDIE, Style.DRAME);
        for(int i = 0 ; i < lesStyles.size(); i++){
            if( lesStyles.get(i) == style){
                return lesQualites.get(i);
            }
        }
        return 0;
    }

    public String citationStyle(Style style){
        List<String> lesCitations = Arrays.asList(getCitationComedie(), getCitationTragedie(), getCitationDrame());
        List<Style> lesStyles = Arrays.asList(Style.COMÉDIE, Style.TRAGÉDIE, Style.DRAME);
        for(int i = 0 ; i < lesStyles.size(); i++){
            if( lesStyles.get(i) == style){
                return lesCitations.get(i);
            }
        }
        return " ";
    }
    
    @Override
    public String toString(){
        return "L'honorable " + this.nom;
    }
}
